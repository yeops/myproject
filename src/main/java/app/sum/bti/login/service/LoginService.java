package app.sum.bti.login.service;

import app.sum.bti.login.mapper.LoginMapper;
import app.sum.bti.login.vo.LoginVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final LoginMapper mapper;

    // 사용자 정보 가져오기
    public LoginVO.LoginUserInfo getLoginUserInfo(LoginVO.LoginInfo params) throws SQLException{
        return mapper.getLoginUserInfo(params);
    }


}
