package app.sum.bti.login;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// 인터셉터로 로그인체크하기
@Slf4j // 로그남기기
public class LoginCheckInterceptor implements HandlerInterceptor { //인터셉터가 되려면 HandlerInterceptor 인터페이스 를 상속받는다.

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        log.info("======== start preHandle ========");
        System.out.println("Intercepted URL: " + request.getRequestURI());
        boolean isPassed = false;

        if(request.getSession().getAttribute("loginUserInfo") == null){
            isPassed = false;
            response.sendRedirect("/login/error");
        }else{
            isPassed = true;
        }

        return isPassed; //false면 cotroller로 진행이 안됨
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("======== start postHandle ========");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("======== start afterCompletion ========");
    }
}
