package app.sum.bti.sign.service;

import app.sum.bti.common.Utils;
import app.sum.bti.sign.mapper.SignMapper;
import app.sum.bti.sign.vo.SignVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class SignService {

    private final SignMapper mapper;
    private final ResourceLoader resourceLoader;

    @Value("${server.stored.img.path}")
    private String imagePath;

    @Value("${server.stored.tempImg.path}")
    private String tempImgPath;

    // 아이디 중복 확인
    public int checkEqualId(Map<String,Object> checkId) throws SQLException {
        return mapper.checkEqualId(checkId);
    }

    // 닉네임 중복 확인
    public int checkEqualNick(Map<String,Object> checkNick) throws SQLException {
        return mapper.checkEqualNick(checkNick);
    }

    public int signCheck(SignVO.SignInfo singInfo) throws SQLException {
        Map<String,Object> param = new HashMap<String,Object>();

        List<String> lMbti = singInfo.getLoveChooseMBTI();
        List<String> hMbti = singInfo.getHateChooseMBTI();
        String defaultValue ="";
        if(lMbti != null) {
            // StringBuilder를 사용하여 문자열을 연결
            StringBuilder result = new StringBuilder();
            for (String str : lMbti) {
                result.append(str);
            }
            singInfo.setLoveMbti(result.toString());
        }

        if(hMbti != null) {
            StringBuilder result2 = new StringBuilder();
            for (String str : hMbti) {
                result2.append(str);
            }
            singInfo.setHateMbti(result2.toString());
        }

        return mapper.signInsert(singInfo);
    }


    public int addPicture(SignVO.Request request) throws Exception{
        int result = 0;
        // 파일 업로드 및 리사이즈 사진 생성 후 객체 반환한 것을 받는다.
        SignVO.PictureInfo pictureInfo = this.uploadPicture(request);

        if(pictureInfo != null){
            result = mapper.uploadPicture(pictureInfo);
        }
        return result;
    }

    private SignVO.PictureInfo uploadPicture(SignVO.Request request) throws Exception {

        MultipartFile file = request.getProfilePicture();
        log.info("File Name: " + file.getOriginalFilename());
        log.info("File Size: " + file.getSize());


        // 사진 정보 객체 생성
        SignVO.PictureInfo pictureInfo = new SignVO.PictureInfo();

        if(file != null && !file.isEmpty()){
            // 파일 원본 이름 가져오기
            String originFileName = file.getOriginalFilename();
            // 파일 확장자
            String ext = originFileName.substring(originFileName.lastIndexOf(".") + 1);
            // 파일 저장할 이름을 만들기 위해 UUID 사용
            String uuid = UUID.randomUUID().toString().replaceAll("-",  "").substring(0, 12);

            String storedFileName = uuid +"." + ext;
            //파일 이름을 포함한 전체 경로
            String fullPath = imagePath + storedFileName;

            //저장할 경로를 가지고 파일객체 만들기
            File newFile = new File(fullPath);
            //파일을 저장할 경로가 없다면!
            if(!newFile.getParentFile().exists()) {
                //경로를 만든다
                newFile.getParentFile().mkdirs();
            }

            newFile.createNewFile(); // 빈 파일 생성
            file.transferTo(newFile);  // 기존 파일내용을 새로운 파일객체에 쓴다

            //이미지 리사이즈 후 이름 값 얻기
            String thumbFileName =  Utils.ResizeFile(200,100,  newFile,  imagePath);
            String thumbPath = imagePath + "thumb" + File.separator ;

            // 생성된 파일정보를 객체에 넣고 list에 저장
            pictureInfo.setFileName(storedFileName);
            pictureInfo.setFilePath(imagePath);
            pictureInfo.setRefileName(thumbFileName);
            pictureInfo.setRefilePath(thumbPath);
            pictureInfo.setUserId(request.getUserId());

        }
        return pictureInfo;
    }

    // 회원정보 삭제
    public void deleteInfo(String userId) throws Exception {
         mapper.deleteInfo(userId);
    }
}